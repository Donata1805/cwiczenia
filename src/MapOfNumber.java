import java.util.HashMap;
import java.util.Map;

public class MapOfNumber {
    public static void main(String[] args) {
        Map<Integer, Boolean> mapOfNumbers = new HashMap<>();
        mapOfNumbers.put(1, false);
        mapOfNumbers.put(2, true);
        mapOfNumbers.put(3, true);
        mapOfNumbers.put(4, true);
        mapOfNumbers.put(5, true);
        mapOfNumbers.put(6, true);
        mapOfNumbers.put(7, true);
        mapOfNumbers.put(8, true);
        mapOfNumbers.put(9, true);
        mapOfNumbers.put(10, true);
        mapOfNumbers.put(11, true);
        mapOfNumbers.put(12, true);
        mapOfNumbers.put(13, true);
        mapOfNumbers.put(14, true);
        mapOfNumbers.put(15, true);

        for(Map.Entry<Integer, Boolean> entry : mapOfNumbers.entrySet()){
            System.out.println(entry);
        }
    }

}
