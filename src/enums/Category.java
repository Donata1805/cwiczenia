package enums;

public enum Category {
    FOOD,
    CLOTHES,
    ALCOHOL,
    TOBACCO;

}
