package enums;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OrderMain {
    public static void main(String[] args) {

        List<Order> orders = new ArrayList<>();
        orders.add(new Order(Status.NEW, LocalDate.now()));
        Order order = new Order(Status.NEW,LocalDate.of(2017,8,16));
        Order order1 = new Order(Status.NEW,LocalDate.of(2017,8,18));
        Order order2 = new Order(Status.NEW,LocalDate.of(2017,8,20));
        Order order3 = new Order(Status.NEW,LocalDate.of(2017,8,22));
        Order order4 = new Order(Status.NEW,LocalDate.of(2017,8,24));
        orders.add(order);
        orders.add(order1);
        orders.add(order2);
        orders.add(order3);
        orders.add(order4);
        for (Order o : orders){
            System.out.println(o.toString());
        }
        LocalDate sevenDaysAgo = LocalDate.now().minusWeeks(1);
        for (Order o: orders){
            if (o.getDate().isBefore(sevenDaysAgo)){
                o.setStatus(Status.OLD);
            }
        }

    }
}
