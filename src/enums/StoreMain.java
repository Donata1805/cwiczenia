package enums;

import java.util.List;

public class StoreMain {
    public static void main(String[] args) {
        Store store = new Store();
        store.add("Masło", 6 , Category.FOOD);
        store.add("Chleb", 2 , Category.FOOD);
        store.add("Piwo",3,Category.ALCOHOL);

        Product product = store.get("Masło");
        System.out.println(product.getName());

        List<Product> food = store.getAll(Category.FOOD);
        for (Product p : food) {
            System.out.println(p.getName());
        }
    }
}
