package enums;

import java.time.LocalDate;

public class Order {
    private Status status;
    private LocalDate date;

    public Order(Status status, LocalDate date) {
        this.status = status;
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Order{" +
                "status=" + status +
                ", date=" + date +
                '}';
    }
}
