package enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Store {
    private final Map<String,Product>products = new HashMap<>();

    public void add (String name,int price,Category category){
        Product product = new Product(name,price,category);
        products.put(name,product);

    }

    public void remove(String name) {
        products.remove(name);
    }

    public Product get(String name) {
        return products.get(name);
    }

    public List<Product> getAll(Category category) {
        List<Product> result = new ArrayList<>();

        for (Product product : products.values()){
            if (product.getCategory() == category){
                result.add(product);
            }
        }
        return result;
    }

}

