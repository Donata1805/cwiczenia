package pl.sda.java.przelacznik.Powitanie;

public class OsobaMain {
    public static void main(String[] args) {
        Osoba jan = new Osoba("Jan",20);
        Osoba zbyszek = new Osoba("Zbyszek",18);
        Osoba ania = new Osoba("Ania",25);

        Osoba[] osoby = {jan,zbyszek,ania};

        daneOsobowe(osoby);

    }

    private static void daneOsobowe(Osoba[] osoby) {
            for(Osoba osoba : osoby){
                System.out.println(osoba.getImie());
                System.out.println(osoba.getWiek());
            }
    }
}
