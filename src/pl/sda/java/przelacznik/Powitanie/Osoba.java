package pl.sda.java.przelacznik.Powitanie;

public class Osoba {
    private String imie;
    private int wiek;

    public Osoba(String imie,int wiek){
        this.imie = imie;
        this.wiek = wiek;
    }
    public int getWiek(){
        return wiek;
    }
    public String getImie(){
        return imie;
    }

}
