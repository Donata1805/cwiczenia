package pl.sda.java.zadania0;

public class Zadania0 {
    public static void main(String[] args) {
        double a = 5.0;
        double b = 3.0;
        long c = 100L;
        long d = 10L;
        float e = 2F;
        float f = 3F;
        String g = "a";

        System.out.println("suma dwóch liczb to: "+ (a+b));
        System.out.println("różnica dwch liczb to: "+(a-b));
        System.out.println("iloraz dwóch liczb : " + (a/b));
        System.out.println("c-d = " + (c-d));
        System.out.println("e-f= " + (e-f));
        System.out.println("g+b= " + (g+b));
        System.out.println("g+b= " + (g+b));
        System.out.println("cos".equals("cos"));
        System.out.println("cos" == "cos");
        System.out.println("cos" == "cos");
        System.out.println("cos".equals("cos"));
        System.out.println("cos".equals("cos"));
        System.out.println(false == false);
        System.out.println(false != true);
        System.out.println("2 + 3 = " + (2 + 3));
        System.out.println("100L - 10 = " + (100L - 10));
        System.out.println();
        System.out.println(3 < 5);
        System.out.println(3 == 3 && 3 == 4);
        System.out.println(3 != 5 || 3 == 5);

    }
}
