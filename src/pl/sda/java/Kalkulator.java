package pl.sda.java;

import java.util.Scanner;

public class Kalkulator {
    public static void main(String[] args) {
        System.out.println("Kalkulator");
        Scanner scanner = new Scanner(System.in);
        System.out.println("wybierz znak działania + - * /");
        String znakDziałania = scanner.nextLine();
        System.out.println("podaj pierwszą liczbę: ");
        int liczba1 = scanner.nextInt();
        System.out.println("podaj drugą liczbę: ");
        double liczba2 = scanner.nextDouble();
        switch(znakDziałania){
            case("+"):
                System.out.println("liczba 1 + liczba 2 = "+(liczba1 + liczba2));
                break;
            case("-"):
                System.out.println("liczba 1 - liczba 2 = "+(liczba1 - liczba2));
                break;
            case("*"):
                System.out.println("liczba 1 * liczba 2 = "+(liczba1 * liczba2));
                break;
            case("/"):
                System.out.println("liczba 1 / liczba 2 = "+(liczba1 / liczba2));

        }
    }
}
