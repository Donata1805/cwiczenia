package pl.sda.java.cwiczenia;

public class Metody2 {
    public static void main(String[] args) {
        drukujParzystosciLiczby(6);
        drukujParzystosciLiczby();
    }

    private static void drukujParzystosciLiczby() {
    }

    private static void drukujParzystosciLiczby(int liczba) {
        if (liczba%2 ==0){
            System.out.println("liczba parzysta");
        }
        else{
            System.out.println("liczba nieparzysta");
        }
    }
}
