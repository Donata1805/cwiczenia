package pl.sda.java.cwiczenia;

public class Metody3 {
    public static void main(String[] args) {
        powiedzCzesc("Donata");
        String zbyszek = "Zbyszek";
        powiedzCzesc(zbyszek);
    }

    private static void powiedzCzesc(String imie) {
        System.out.println("Mam na imię :" + imie);
    }
}
