package pl.sda.java.cwiczenia;

import java.util.Scanner;

public class Metody4 {
    public static void main(String[] args) {
        int wprowadzonaLiczba = wczytajLiczbe();
        System.out.println(wprowadzonaLiczba);

        if (wprowadzonaLiczba%3==0){
            System.out.println("czy liczba jest podzielna przez 3");
        }
        else {
            System.out.println("czy liczba jest podzielna przez 5");
        }
    }

    private static int wczytajLiczbe() {
        Scanner scanner = new Scanner(System.in);
        int wczytanaLiczba = scanner.nextInt();
        return wczytanaLiczba;
    }
}
