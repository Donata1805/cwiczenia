package pl.sda.java;

public class TestIf {
    public static void main(String[] args) {
        int zmienna1 = 2;
        int zmienna2 = 4;

        if(zmienna1>3)
            System.out.println(":-)");
        else
            System.out.println(":-(");

        if(zmienna2<5)
            System.out.println(":-)");
        else
            System.out.println(":-(");

        if((2-2)==0)
            System.out.println(":-)");
        else
            System.out.println(":-(");
    }
}
