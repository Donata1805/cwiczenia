package pl.sda.java;

import java.util.Scanner;

public class TestSkanner {
    public static void main(String[] args) {
        System.out.println("podaj liczbę: ");

        Scanner scanner = new Scanner(System.in);
        int liczba = scanner.nextInt();
        if(liczba%3 == 0 || liczba%5 == 0) {
            System.out.println("liczba jest podzielna przez 3 lub 5");
        }else{
            System.out.println("liczba nie jest podzielna przez 3 ani przez 5");
        }
    }
}
