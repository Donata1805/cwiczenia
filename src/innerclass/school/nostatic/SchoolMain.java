package innerclass.school.nostatic;

public class SchoolMain {
    public static void main(String[] args) {
        School school = new School("III LO ", " Kazimerza Wielkiego");
        School.Pupil pupil = school.newPupil("Jan");
        pupil.introduce();
    }
}