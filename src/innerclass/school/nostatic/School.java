package innerclass.school.nostatic;

public class School {
    private String name;
    private String patron;

    public Pupil newPupil(String name){
        return new Pupil(name);
    }
    public School(String name, String patron){
        this.name = name;
        this.patron = patron;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatron() {
        return patron;
    }

    public void setPatron(String patron) {
        this.patron = patron;
    }
    public class Pupil {
        private String name;

        public Pupil(String name){
            this.name = name;
        }
        public void introduce(){
            System.out.println("Nazywam się " + name + " ," + "uczęszczam do szkoły "+ School.this.name + "imienia" + patron);
        }
    }

}
